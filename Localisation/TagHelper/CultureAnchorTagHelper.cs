﻿using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Options;

namespace Localisation.TagHelper
{
    [HtmlTargetElement("a", Attributes = ACTION_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = CONTROLLER_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = AREA_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = PAGE_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = PAGE_HANDLER_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = FRAGMENT_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = HOST_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = PROTOCOL_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = ROUTE_ATTRIBUTE_NAME)]
    [HtmlTargetElement("a", Attributes = ROUTE_VALUES_DICTIONARY_NAME)]
    [HtmlTargetElement("a", Attributes = ROUTE_VALUES_PREFIX + "*")]
    public class CultureAnchorTagHelper : AnchorTagHelper
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly string _defaultRequestCulture = "vi";

        private const string ACTION_ATTRIBUTE_NAME = "asp-action";
        private const string CONTROLLER_ATTRIBUTE_NAME = "asp-controller";
        private const string AREA_ATTRIBUTE_NAME = "asp-area";
        private const string PAGE_ATTRIBUTE_NAME = "asp-page";
        private const string PAGE_HANDLER_ATTRIBUTE_NAME = "asp-page-handler";
        private const string FRAGMENT_ATTRIBUTE_NAME = "asp-fragment";
        private const string HOST_ATTRIBUTE_NAME = "asp-host";
        private const string PROTOCOL_ATTRIBUTE_NAME = "asp-protocol";
        private const string ROUTE_ATTRIBUTE_NAME = "asp-route";
        private const string ROUTE_VALUES_DICTIONARY_NAME = "asp-all-route-data";
        private const string ROUTE_VALUES_PREFIX = "asp-route-";
        private const string HREF = "href";

        public CultureAnchorTagHelper(IHttpContextAccessor contextAccessor, IHtmlGenerator generator) :
          base(generator)
        {
            _contextAccessor = contextAccessor;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var culture = (string)_contextAccessor.HttpContext.Request.RouteValues["culture"];
            //IList<System.Globalization.CultureInfo> supportedCultures = _options.Value.SupportedCultures;
            //IEnumerable<string> enumerable = supportedCultures.Select(x => x.Name);

            if (culture != null && culture != _defaultRequestCulture)
                RouteValues["culture"] = culture;

            base.Process(context, output);
        }
    }
}
