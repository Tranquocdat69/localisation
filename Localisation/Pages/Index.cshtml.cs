﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Localisation.Pages
{
    public class IndexModel : PageModel
    {
        [BindProperty(SupportsGet = true, Name = "pageUrl")]
        public string PageUrl { get; set; }
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            string culture = (string) HttpContext.Request.RouteValues["culture"];
        }
    }
}