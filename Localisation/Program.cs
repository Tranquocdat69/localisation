using System.Globalization;
using System.Reflection;
using Localisation.Resources;
using Localisation.RouteModelConventions;
using Localisation.Services;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization.Routing;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddMvc()
    .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
    .AddDataAnnotationsLocalization(options =>
    {
        options.DataAnnotationLocalizerProvider = (type, factory) =>
        {
            var assemblyName = new AssemblyName(typeof(CommonResources).GetTypeInfo().Assembly.FullName);
            return factory.Create(nameof(CommonResources), assemblyName.Name);
        };
    });
builder.Services.AddRazorPages(options => {
    options.Conventions
        //.AddPageRoute("/Index", "{*url}")
        .AddPageRoute("/Sample/Account/Index", "thong-tin-thanh-vien")
        .AddPageRoute("/Sample/Account/Register", "dang-ki-thanh-vien");
    options.Conventions.Add(new CultureTemplatePageRouteModelConvention());
});
builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");
builder.Services.Configure<RequestLocalizationOptions>(options =>
{
    var supportedCultures = new[]
    {
        //new CultureInfo("vi-VN"),
        //new CultureInfo("en-GB"),
        //new CultureInfo("en-US"),
        //new CultureInfo("ja-JP"),
        new CultureInfo("vi"),
        new CultureInfo("en"),
        new CultureInfo("ja"),
    };
    options.DefaultRequestCulture = new RequestCulture("vi");
    options.SupportedCultures = supportedCultures;
    options.SupportedUICultures = supportedCultures;
    options.RequestCultureProviders.Insert(0, new RouteDataRequestCultureProvider { Options = options });
    //options.RequestCultureProviders.Insert(1, new CustomRequestCultureProvider(context =>
    //{
    //    //var userLangs = context.Request.Headers["Accept-Language"].ToString();
    //    //var firstLang = userLangs.Split(',').FirstOrDefault();
    //    var defaultLang = "vi";/* string.IsNullOrEmpty(firstLang) ? "en" : firstLang;*/
    //    return Task.FromResult(new ProviderCultureResult(defaultLang, defaultLang));
    //}));
    //options.AddInitialRequestCultureProvider(new CustomRequestCultureProvider(async context =>
    //{
    //    // My custom request culture logic
    //    return await Task.FromResult(new ProviderCultureResult("vi-VN"));
    //}));
});
builder.Services.AddSingleton<CommonLocalizationService>();
builder.Services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

var localizationOptions = app.Services.GetService<IOptions<RequestLocalizationOptions>>().Value;
app.UseRequestLocalization(localizationOptions);

app.UseAuthorization();

app.MapRazorPages();

app.Run();
